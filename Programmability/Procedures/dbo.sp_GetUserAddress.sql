﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
 
-- New procedure
CREATE PROCEDURE [dbo].[sp_GetUserAddress]
AS
BEGIN
	SELECT Lastname
		,Address
	FROM UserAddress
END
GO