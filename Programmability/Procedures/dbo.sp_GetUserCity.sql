﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_GetUserCity]
AS
BEGIN
  SELECT
    UserAddress.FirstName
   ,UserAddress.Lastname
   ,Address.City
  FROM UserAddress
  INNER JOIN Address
    ON UserAddress.AddresID = Address.UserAddressID
END

GO