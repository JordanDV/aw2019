﻿CREATE TABLE [dbo].[Person] (
  [BusinessEntityID] [int] NULL,
  [PersonType] [nvarchar](50) NULL,
  [NameStyle] [bit] NULL,
  [Title] [nvarchar](50) NULL,
  [FirstName] [nvarchar](50) NULL,
  [MiddleName] [nvarchar](50) NULL,
  [LastName] [nvarchar](50) NULL,
  [Suffix] [nvarchar](50) NULL,
  [EmailPromotion] [int] NULL,
  [AdditionalContactInfo] [varbinary](max) NULL,
  [Demographics] [varbinary](max) NULL,
  [rowguid] [nvarchar](50) NULL,
  [ModifiedDate] [datetime2] NULL
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO