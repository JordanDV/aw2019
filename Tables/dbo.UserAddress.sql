﻿CREATE TABLE [dbo].[UserAddress] (
  [AddresID] [int] IDENTITY,
  [FirstName] [varchar](100) NULL,
  [Lastname] [varchar](150) NULL,
  [Address] [varchar](250) NULL,
  PRIMARY KEY CLUSTERED ([AddresID])
)
ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- New Trigger
CREATE TRIGGER [dbo].[trgAfterInsert] ON [dbo].[UserAddress]
FOR INSERT
AS
PRINT 'Data entered successfully'
GO