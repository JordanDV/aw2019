﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
-- New View
CREATE VIEW [dbo].[v_Address]
AS
SELECT ID
	,City
	,PostalCode
	,UserAddressID
FROM dbo.Address
GO